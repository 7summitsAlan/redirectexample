@IsTest
private class PageRedirect_Test {

    private static Map<String,String> redirectMap = new Map<String,String>{
        '/page/one' => '/aaaa1',
        '/page/two' => '/aaaa2',
        '/wild/one' => '/bbb1',
        '/wild/*' => '/cccc'
    };

    @TestSetup
    static void setup() {
        List<PageRedirect__c> redirects = new List<PageRedirect__c>();
        for(String k : redirectMap.keySet()) {
            PageRedirect__c pr = new PageRedirect__c();
            pr.OriginalURL__c = k;
            pr.Target__c = redirectMap.get(k);
            redirects.add(pr);
        }
        insert redirects;
    }
    
    @IsTest
    static void checkUrlRedirection() {
        PageRedirect pr = new PageRedirect();
        String url1 = '/page/one';
        String url2 = '/page/two';
        String url3 = '/wild/one';
        String url4 = '/wild/random';
        String url5 = '/bad/url';
        PageReference pageRef = new PageReference(url1); 
        Test.setCurrentPage(pageRef);
        PageReference result = pr.urlRedirection();
        System.assertEquals(redirectMap.get(url1), result.getUrl());
        pageRef = new PageReference(url2); 
        Test.setCurrentPage(pageRef);
        result = pr.urlRedirection();
        System.assertEquals(redirectMap.get(url2), result.getUrl());
        pageRef = new PageReference(url5); 
        Test.setCurrentPage(pageRef);
        result = pr.urlRedirection();
        System.assertEquals('/s/error', result.getUrl());
    }

}