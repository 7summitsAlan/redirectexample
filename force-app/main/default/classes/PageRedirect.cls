/*
 * PageRedirect Class
 * Controller class for the VF page 
 *    The urlRedirection method determines the requested URL and then searches the Page_Redirect__c custom
 *    object for records with a matching OriginalUrl__c field. If found, it then uses the Target__c field
 *    to perform a redirect to the specified URL. If the URL is not found, then the browser is redirected
 *    to the defaultErrorPage.
 *
 *    Records in the Page_Redirect__c custom object can be exact URLs (e.g. /path/to/my/page.html), URLs
 *    with wildcards (e.g. /path/to/my/*). A root wildcard (/*) will provide a way to default to something
 *    other than the defaultErrorPage
*/
public without sharing class PageRedirect {

    // set the default error page (used if the target is not found)
    private static String defaultErrorPage = '/s/error';

    // method to perform the actual redirect
    public PageReference urlRedirection() {
        // page reference to be returned as part of the redirect
        PageReference page;
        // retrieve the list of the search patterns to use
        List<String> searchPatterns = this.getSearchPatterns();
        // query the PageRedirect object using the search patterns. Results are ordered in descending length
        // so the longest one (which will be the original URL requested) will be first
        List<PageRedirect__c> redirectList = [SELECT Id,Target__c,OriginalUrl__c,HTTP_Redirect_Code__c FROM PageRedirect__c WHERE OriginalUrl__c IN :searchPatterns ORDER BY Url_Length__c DESC];
        // if the redirectList has at least one entry, we have a target!
        if(redirectList.size() > 0) {
            String target = redirectList[0].Target__c;
            Integer redirectCode = Integer.valueOf(redirectList[0].HTTP_Redirect_Code__c);
            // if the original URL had parameters passed to it, pass those along to the target
            String originalParams = getRequestParameters();
            if(!String.isBlank(originalParams)) {
                // if the target URL already has a query string, append to it
                // otherwise build a new query string
                if(target.contains('?')) {
                    target += '&';
                } else {
                    target += '?';
                }
                target += originalParams;
            }
            // set the target and redirect code from the first element in the results
            page = new PageReference(target);
            page.setRedirectCode(redirectCode);
        } else {
            // set the page reference to the default error page
            page = new PageReference(defaultErrorPage);
        }
        // set the page reference as a redirect
        page.setRedirect(true);
        // return to the VF page for redirection
        return page;
    }
    
    // helper method to get a list of the search patterns to be used in the query
    // the requested url is broken into its path parts, and the array of patterns is built based on the parts
    //
    // so if the requested URL was /path/to/some/old/location, then the search patterns
    // returned would be:
    //     /path/to/some/old/location, /path/to/some/old/*, /path/to/some/*, /path/to/*, /path/*
    private List<String> getSearchPatterns() {
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        String currentRequestURL = URL.getCurrentRequestUrl().toExternalForm();
        if(Test.isRunningTest()) {
            currentRequestURL = ApexPages.currentPage().getUrl();
        }
        // remove the SFDC base url and /apex from the requested URL, if present
        String requestedUrl = currentRequestURL.replace(sfdcBaseURL, '').replaceFirst('^/apex', '').substringBeforeLast('?');
        String[] parts = requestedUrl.split('/');
        List<String> patterns = new List<String>();
        patterns.add(requestedUrl);
        for(Integer i=parts.size()-1; i>1; i--) {
            List<String> sublist = new List<String>();
            for(Integer j=0;j<i;j++) {
                sublist.add(parts[j]);
            }
            patterns.add(String.join(sublist, '/') + '/*');
        }
        patterns.add('/*');
        return patterns;
    }

    // helper method to get the query string from the original URL
    private String getRequestParameters() {
        String currentRequestURL = URL.getCurrentRequestUrl().toExternalForm();
        return currentRequestURL.substringAfterLast('?');
    }
}